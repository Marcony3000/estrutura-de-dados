
package Fila;
import Colecao.ColecaoNaoComparavel;
import projeto_ed.NoSimpEnc;
import ListaEnc.ListaSimpEnc;
import projeto_ed.MyIterator;
/**
 *
 * @author 20151LBSI120040
 */
public class FilaEnc<E> extends Fila<E>{

    private ListaSimpEnc<E> fila;
    private NoSimpEnc<E> inicio;
    private NoSimpEnc<E> fim;
    private int qntMaxima; 
    
    @Override
    public boolean insira(E obj) {
       fila.insertAtEnd(obj);
      return true;
    }

    @Override
    public E remova() {
       return fila.removeFromBegin();
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MyIterator<E> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[] toarray() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
