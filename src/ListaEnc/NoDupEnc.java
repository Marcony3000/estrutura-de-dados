/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_ed;

/**
 *
 * @author Ana Carla
 */
public class NoDupEnc<E> {
    
    private E obj;
    private NoDupEnc<E> ant;
    private NoDupEnc<E> prox;

    //Cria nó cabeça
    public NoDupEnc() {
        this.ant = this;
        this.prox = this;
    }

    //Cria 1º nó e adiciona na lista
    public NoDupEnc(E obj, NoDupEnc<E> ant, NoDupEnc<E> prox) {
        this.obj = obj;
        this.ant = ant;
        ant.prox = this;
        this.prox = prox;
        prox.ant = this;
    }
    
    //Remove um nó da lista
    public void remove(){
        this.ant.prox = this.prox;
        this.prox.ant = this.ant;
}

    public E getObj() {
        return obj;
    }

    public void setObj(E obj) {
        this.obj = obj;
    }

    public NoDupEnc<E> getAnt() {
        return ant;
    }

    public void setAnt(NoDupEnc<E> ant) {
        this.ant = ant;
    }

    public NoDupEnc<E> getProx() {
        return prox;
    }

    public void setProx(NoDupEnc<E> prox) {
        this.prox = prox;
    }
    
    
    
    
}
