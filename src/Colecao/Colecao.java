/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecao;

import java.io.Serializable;
import projeto_ed.MyIterator;

/**
 *
 * @author 20151lbsi120040
 */
public interface Colecao<E> extends Serializable{
    
    int size();
    
    boolean isEmpty();
    
    void clear();
    
   MyIterator<E> iterator();
    
    Object[] toarray();
}
