package projeto_ed;

/**
 * Created by gilson on 07/12/16.
 */
public class MyVetorIterator<E> extends MyIterator<E> {

    protected int posicao;
    protected Vetor<E> lista;


    public MyVetorIterator(Vetor<E> obj) {
        this.posicao = 0;
        this.lista = obj;
    }

    @Override
    public boolean hasNext() {
        if (posicao == lista.numItens)
            return false;

        return true;
    }

    @Override
    public E next() {
        E obj = this.lista.elementAt(posicao);
        posicao++;
        return obj;
    }

    @Override
    public Object getFirst() {
        return this.lista.elementAt(0);
    }

    @Override
    public Object getNext() {
        if (hasNext())
            return next();
        return null;
    }

}
