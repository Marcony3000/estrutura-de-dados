/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListaEnc;

/**
 *
 * @author Ana Carla
 */
public abstract class ListaSimpEnc<E>  {
    
    protected NoSimpEnc<E> inicio;
    protected NoSimpEnc<E> fim;
    protected int numItens;
    protected NoSimpEnc<E> p;
    
    public NoSimpEnc<E> getInicio(){
        return inicio;
    }
    
    public NoSimpEnc<E> getFim(){
        return fim;
    }
    
    public void clear(){
        this.inicio=null;
        this.fim=null;
        for (int i = 0; i < numItens; i++) {
            p=null;
        }
    }
    
    public int size(){
      return  numItens;
    }
    
    public int buscaElement(E obj){
        NoSimpEnc<E> busca = inicio;
        for (int i = 0; i < numItens; i++) {
            if(busca.getObj.equals(obj)){
                System.out.println("Objeto encontrado! ");
                return i;
            }
            busca = busca.getProx;
        }
        System.out.println("Objeto não detectado!");
        return -1;
    }
    
    public boolean isEmpty(){
        if(fim == null && p == null && inicio==null){
            return true;
        }else{
            return false;
        }
    }
    
    public void insertAtBegin(E obj) {
         NoSimpEnc<E> NovoNo = new NoSimpEnc<E>(obj);
         if(inicio==null){
             inicio = fim = NovoNo;
         }else{
             NovoNo.setProx(inicio);
             inicio = NovoNo;
         }
          numItens++;
     }
     
    public void insertAtEnd(E obj) {
        NoSimpEnc<E> NovoNo = new NoSimpEnc<E>(obj);
         if(inicio==null){
             inicio = fim = NovoNo;
         }else{
             fim.setProx(NovoNo);
             fim = NovoNo;
         }
         numItens++;
      }
    
    public void insertAfter(E obj, NoSimpEnc<E> p){
         NoSimpEnc<E> NovoNo = new NoSimpEnc<E>(obj, p.getProx());
         if(p== fim){
             fim=NovoNo;
         }p.setProx(NovoNo);
         numItens++;
    }
    
    public E removeFromBegin(){
        inicio = inicio.getProx(); 
        if (inicio == null){
            fim = null;
        }
        numItens--;
        return (E) getInicio();
    }
    
     public E removeAfter(NoSimpEnc<E> p) {
         if(p.getProx() ==  fim){
             fim = p;
         }
         p.setProx(p.getProx().getProx());
         numItens--;
         return (E) p;
     }
     
   
}
