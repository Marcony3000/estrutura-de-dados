package  projeto_ed;
import java.io.Serializable;

/**
 *
 * @author 20151lbsi120040
 */
public class Vetor<E> implements Serializable {

    //Valor inicial do tamanho do array (default)
    private final int CAPACIDADE_DEFAULT = 100;
    //Array onde os dados serão armazenados
    protected E[] lista;

    //Valores informados pelo usuário para a capacidade
    //inicial e para o incremento a ser utilizado quando
    //o array for redimensionado
    protected int incremento, capacidadeInicial;
    //Total de dados armazenados no array
    protected int numItens=0;

    //Construtores
    //Métodos
    public Vetor() {
        lista = (E[]) new Object[CAPACIDADE_DEFAULT];
        incremento = 10;
        capacidadeInicial = CAPACIDADE_DEFAULT;
    }

    public Vetor(int capacidadeInicial) {
        lista = (E[]) new Object[capacidadeInicial];
        this.incremento = 10;
        this.capacidadeInicial = capacidadeInicial;
    }

    public Vetor(int capacidadeInicial, int incremento) {
        lista = (E[]) new Object[capacidadeInicial];
        this.incremento = incremento;
        this.capacidadeInicial = capacidadeInicial;
    }

    public void clear() {
        for (int i = 0; i < lista.length; i++) {
            lista[i] = null;
        }
        numItens = 0;
        lista = (E[]) new Object[capacidadeInicial];
    }

    public void insertAtEnd(E obj) {
        if (numItens == lista.length) {
            redimencionarVetor();

        }
        lista[numItens] = obj;
        numItens++;

    }

    public boolean insertAt(int indice, E obj) {

        if (indice < 0 && indice > lista.length) {
            System.out.println("Indice inv�lido");
            return false;
        }
        if (numItens == lista.length) {
            redimencionarVetor();

        }

        E aux, aux2 = null;
        for (int i = indice; i < lista.length; i++) {
            aux = lista[i];
            lista[i] = aux2;
            aux2 = aux;
        }
        lista[indice] = obj;
        return true;

    }

    public void insertAtBegin(E obj) {
        insertAt(0, obj);
    }

    public E removeFromEnd() {
        if (isEmpty()) {
            return null;
        }
        E aux = lista[numItens - 1];
        lista[numItens] = null;
        numItens--;
//            for (int i = 0; i < lista.length; i++) {
//                if (i == lista.length) {
//                    aux = lista[i];
//                    lista[i] = null;
//                }
//            }
        return aux;

    }

    public E removeAt(int indice) {

        if (isEmpty() || indice < 0 || indice > lista.length) {
            System.out.println("Indice Invalido!");
            return null;
        } else {
            E aux = lista[indice];

            for (int j = indice; j < numItens; j++) {
                lista[j] = lista[j + 1];
            }
            lista[numItens - 1] = null;
            numItens--;
            
            return aux;
        }
    }

    public E removeFromBegin() {
        return removeAt(0);
    }

    public boolean replace(int indice, E obj) {
        if (indice < 0 && indice > lista.length) {
            System.out.println("Indice Inv�lido");
            return false;
        } else {
            lista[indice] = obj;
            return true;
        }

    }
    

    public E elementAt(int indice) {

        if (indice < 0 && indice > lista.length) {
            System.out.println("Indice Inv�lido");
            return null;
        } else {
            return lista[indice];
        }
    }

    public boolean isEmpty() {

        if (lista.length == 0) {
            System.out.println("Vetor vazio!");
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        return lista.length;
    }

    public void redimencionarVetor() {

        E[] temp = (E[]) new Object[lista.length + incremento]; //Criando um arrayObeject temporario
        System.arraycopy(lista, 0, temp, 0, lista.length);//Copiando o array lista para o array temp
        for (int j = 0; j < lista.length; j++) {
            lista[j] = temp[j]; //Copiando o arrayObject de temp para lista //lista=temp;
        }
    }

    public MyIterator<E> iterator() {

        return null;

    }

}
