
package Fila;
import Colecao.ColecaoNaoComparavel;

/**
 *
 * @author 20151LBSI120040
 */
public abstract class Fila<E> extends ColecaoNaoComparavel<E>{
    
    public abstract boolean insira(E obj);
    public abstract E remova();
}
