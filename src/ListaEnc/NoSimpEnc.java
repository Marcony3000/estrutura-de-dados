/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_ed;

/**
 *
 * @author Ana Carla
 */
public class NoSimpEnc<E> {
    
    private E obj;
    private NoSimpEnc<E> prox;

    public NoSimpEnc(E obj) {
        this.obj = obj;
        this.prox = null;
    }

    public NoSimpEnc(E obj, NoSimpEnc<E> prox) {
        this.obj = obj;
        this.prox = prox;
    }

    public E getObj() {
        return obj;
    }

    public void setObj(E obj) {
        this.obj = obj;
    }

    public NoSimpEnc<E> getProx() {
        return prox;
    }

    public void setProx(NoSimpEnc<E> prox) {
        this.prox = prox;
    }
     
       
    
}
