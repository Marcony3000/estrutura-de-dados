
package Fila;
import projeto_ed.MyIterator;
import projeto_ed.Vetor;

/**
 *
 * @author 20151LBSI120040
 */
public class FilaArray<E> extends Fila<E>{

    private Vetor<E> fila;
    
   public FilaArray(int tamMaximo){
       fila = new Vetor<>(tamMaximo);
   }
    
    
    @Override
    public boolean insira(E obj) {
        
      fila.insertAtEnd(obj);
      return true;
    }

    @Override
    public E remova() {
       return fila.removeAt(0);
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MyIterator<E> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[] toarray() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

    
    
    
}
