package Colecao;
import projeto_ed.MyIterator;

/**
 *
 * @author 20151lbsi120040
 * @param <E>
 */
public abstract class ColecaoNaoComparavel<E> implements Colecao<E> {

    protected int numItens;

    @Override
    public boolean isEmpty() {
        return (numItens == 0);
    }

    @Override
    public abstract int size();

    public Object[] toArray() {

        if (isEmpty()) {
            return null;
        }
        Object[] objs = new Object[numItens];
        MyIterator<E> it = iterator();
        int i = -1;
        Object obj = it.getFirst();
        while (obj != null) {
            i++;
            objs[i] = obj;
            obj = it.getNext();
        }
        return objs;
    }
}
