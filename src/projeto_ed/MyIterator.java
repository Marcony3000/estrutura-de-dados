package projeto_ed;

import java.util.Iterator;

/**
 * Created by gilson on 07/12/16.
 */
public abstract class MyIterator<E> implements Iterator<E> {

    public abstract Object getFirst();

    public abstract Object getNext();
}
